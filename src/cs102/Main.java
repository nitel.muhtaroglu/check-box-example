package cs102;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class Main {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Simple Frame");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(400, 400);

        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(6, 1));
        frame.add(mainPanel);

        JCheckBox checkBox1 = new JCheckBox("check 1");
        JCheckBox checkBox2 = new JCheckBox("check 2");
        JCheckBox checkBox3 = new JCheckBox("check 3");
        JCheckBox checkBox4 = new JCheckBox("check 4");
        JCheckBox checkBox5 = new JCheckBox("check 5");

        ButtonGroup buttonGroup1 = new ButtonGroup();
        buttonGroup1.add(checkBox1);
        buttonGroup1.add(checkBox2);
        buttonGroup1.add(checkBox3);
        buttonGroup1.add(checkBox4);
        buttonGroup1.add(checkBox5);

        mainPanel.add(checkBox1);
        mainPanel.add(checkBox2);
        mainPanel.add(checkBox3);
        mainPanel.add(checkBox4);
        mainPanel.add(checkBox5);

        JButton button = new JButton("Report");
        mainPanel.add(button);
        button.addActionListener(new ButtonListener(new ArrayList<JCheckBox>(Arrays.asList(checkBox1, checkBox2, checkBox3, checkBox4, checkBox5))));

        frame.setVisible(true);
    }
}
